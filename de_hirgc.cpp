#include <iostream>
#include <vector>
#include <fstream>
#include "chrono"

using namespace std;
using namespace std::chrono;

typedef unsigned long long ll;

const int MAX_ARR_NUM_BIT = 26;
const int MAX_ARR_NUM = 1 << MAX_ARR_NUM_BIT;

// region auxiliary data (delta)
string ID;
typedef struct {
    int sequence_len;
    long num;
} SeqLen;
vector<SeqLen> SEQ_LEN;

typedef struct {
    int pos;
    int len;
} Position;

vector<Position> LOW;
vector<Position> N;

typedef struct {
    int pos;
    char ch;
} Others;
vector<Others> OTH;
// endregion

// the only algortihm parameter, length of the minimal match
int k = 15;

short reference[MAX_ARR_NUM];
short target[MAX_ARR_NUM];

int bucket_head[MAX_ARR_NUM];
int bucket_next[MAX_ARR_NUM];

int reference_length = 0, target_length = 0;

/**
 * Encodes a character to an integer value.
 * @param c character to encode
 * @return -1 if the encoding is not supported, 4 if the character is N
 * @author Mateja Iveta
 */
int encode(char c) {
    switch (c) {
        case 'A':
            return 0;
        case 'C':
            return 1;
        case 'G':
            return 2;
        case 'T':
            return 3;
        case 'N':
            return 4;
        default:
            return -1;
    }
}

/**
 * Decodes an integer to its original value.
 * @param c int to decode.
 * @return letter for this character, or exit if the character is not recognized.
 * @author Mateja Iveta
 */
char decode(int c) {
    switch (c) {
        case 0:
            return 'A';
        case 1:
            return 'C';
        case 2:
            return 'G';
        case 3:
            return 'T';
        default:
            cerr << "Unrecognized token: " << c << endl;
            exit(-1);
    }
}

/**
 * Initializes the delta information.
 * @author Mateja Iveta
 */
void initDelta() {
    SEQ_LEN.clear();
    LOW.clear();
    N.clear();
    OTH.clear();
}

/**
 * Loads the reference sequence from the specified file, preprocesses it and stores the clean reference in a global variable.
 * @param filename reference file
 * @author Mateja Iveta
 */
void loadRefFile(char *filename) {
    auto start = high_resolution_clock::now();

    ifstream input;
    input.open(filename, ios::in);

    if (!input.is_open()) {
        cerr << "Failed to open file '" << filename << "'. Bailing out." << endl;
        exit(-1);
    }

    string L, line;

    if (!getline(input, line)) {
        cout << "No sequence name. Bailing out." << endl;
        exit(-1);
    }

    while (getline(input, line)) {
        if (line.empty()) continue;

        for (char &i : line) {
            if (islower(i)) i = toupper(i);

            int encoded = encode(i);
            if (encoded < 0 || encoded == 4) continue;

            reference[reference_length++] = encoded;
        }
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Loading reference file finished in: " << duration.count() << " ms." << endl;
}

/**
 * Finds the index of a comma in the given string.
 * @param str string to search through.
 * @return -1 if there is no comma in the given string, index of the comma otherwise.
 * @author Luka Mercep
 */
int commaPosition(string str) {
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ',') return i;
    }
    return -1;
}

/**
 * Splits a string with the given delimiter.
 * @param s string to split.
 * @param delimiter string to split by.
 * @return vector of substrings of the given string in between delimiters.
 * @author Mateja Iveta
 */
vector<string> split_string(const string &s, const string &delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

/**
 * Parses the compressed file, reading up to the '%' delimiter for Delta,
 * and the rest of the file contains pairs of matched sequences.
 * @param filename
 * @authors Luka Mercep, Mateja Iveta
 */
void parseCompressed(char *filename) {
    initDelta();

    ifstream input;
    input.open(filename, ios::in);

    // region readDelta
    string line;
    if (!getline(input, line)) {
        cerr << "No sequence name. Bailing out." << endl;
        exit(-1);
    }
    ID = line;

    vector<string> split;

    if (!getline(input, line)) {
        cerr << "No long sequence lengths. Bailing out." << endl;
        exit(-1);
    }
    split = split_string(line, " ");
    for (const string &s : split) {
        if (s.empty()) continue;
        vector<string> pos_split = split_string(s, ",");

        if (pos_split.size() != 2) {
            if (pos_split.size() == 1 && pos_split[0].empty()) continue;
            cerr << "Can't parse sequence len!" << endl;
            exit(-1);
        }

        SeqLen seqLen = {stoi(pos_split[0]), stoi(pos_split[1])};
        SEQ_LEN.push_back(seqLen);
    }
    split.clear();


    if (!getline(input, line)) {
        cerr << "No lowercase letter positions. Bailing out." << endl;
        exit(-1);
    }
    split = split_string(line, " ");
    for (const auto &s : split) {
        vector<string> pos_split = split_string(s, ",");

        if (pos_split.size() != 2) {
            if (pos_split.size() == 1 && pos_split[0].empty()) continue;
            cerr << "Can't parse lowercase letter positions!" << endl;
            exit(-1);
        }

        Position position = {stoi(pos_split[0]), stoi(pos_split[1])};
        LOW.push_back(position);
    }
    split.clear();


    if (!getline(input, line)) {
        cerr << "No letter N positions. Bailing out." << endl;
        exit(-1);
    }
    split = split_string(line, " ");
    for (const auto &s : split) {
        vector<string> pos_split = split_string(s, ",");

        if (pos_split.size() != 2) {
            if (pos_split.size() == 1 && pos_split[0].empty()) continue;
            cerr << "Can't parse letter N positions!" << endl;
            exit(-1);
        }

        Position position = {stoi(pos_split[0]), stoi(pos_split[1])};
        N.push_back(position);
    }
    split.clear();


    if (!getline(input, line)) {
        cerr << "No other characters. Bailing out." << endl;
        exit(-1);
    }
    split = split_string(line, " ");
    for (const auto &s : split) {
        vector<string> pos_split = split_string(s, ",");

        if (pos_split.size() != 2) {
            if (pos_split.size() == 1 && pos_split[0].empty()) continue;
            cerr << "Can't parse other characters!" << endl;
            exit(-1);
        }

        if (pos_split[1].size() != 1) {
            cerr << pos_split[1] << " should be a character!" << endl;
            exit(-1);
        }

        Others oth = {stoi(pos_split[0]), pos_split[1][0]};
        OTH.push_back(oth);
    }
    split.clear();
    // endregion

    getline(input, line); // skip over '%'

    int target_index = 0;

    while (true) {
        getline(input, line);
        if (line[0] == '%') break;

        int commaIndex = commaPosition(line);

        // No comma found, this part of the sequence was not matched
        if (commaIndex == -1) {
            for (char i : line) {
                target[target_index++] = i - '0';
            }

            continue;
        }

        // Comma found, we have a pair (position, length) of a match
        int position = 0;
        for (int i = 0; i < commaIndex; i++) {
            position = position * 10 + (line[i] - '0');
        }
        position--; // indexing from 0

        int length = 0;
        for (int i = commaIndex + 1; i < line.size(); i++) {
            length = length * 10 + (line[i] - '0');
        }

        // Add the matched substring of the reference to the target
        for (int i = position; i < position + length; i++) {
            target[target_index++] = reference[i];
            if (i < 0 || i >= reference_length) {
                cerr << "Out of bounds" << endl;
                exit(0);
            }
        }
    }

    target_length = target_index;
}

/**
 * Restores the original target sequence from the clean target and the delta data.
 * Requires the target sequence and delta information to alredy be in memory, in global variables.
 * @param dest filename in which to store the decompressed sequence
 * @author Mateja Iveta
 */
void decompress(char *dest) {
    auto start = high_resolution_clock::now();

    ofstream output;
    output.open(dest, ios::out);

    output << ">" << ID << endl;

    auto seq_len_it = SEQ_LEN.begin();
    SeqLen seqLen = seq_len_it != SEQ_LEN.end() ? (SeqLen) *seq_len_it++ : SeqLen{-1, -1};
    int sequence_len = seqLen.sequence_len;

    auto low_it = LOW.begin();
    Position low = low_it != LOW.end() ? (Position) *low_it++ : Position{-1, -1};

    auto n_it = N.begin();
    Position n = n_it != N.end() ? (Position) *n_it++ : Position{-1, -1};

    auto oth_it = OTH.begin();
    Others oth = oth_it != OTH.end() ? (Others) *oth_it++ : Others{-1, -1};

    long len = 0;
    for (int i = 0; i <= target_length; i++) {
        while (true) {
            if (seqLen.sequence_len == 0) {
                output << '\n';
                seqLen.num--;

                if (seqLen.num > 0) {
                    seqLen.sequence_len = sequence_len;
                } else {
                    seqLen = seq_len_it != SEQ_LEN.end() ? (SeqLen) *seq_len_it++ : SeqLen{-1, -1};
                    sequence_len = seqLen.sequence_len;
                }
            }

            seqLen.sequence_len--;

            char c = '\0';
            if (oth.pos == len) {
                c = oth.ch;
                oth = oth_it != OTH.end() ? (Others) *oth_it++ : Others{-1, -1};
            } else if (n.pos == len) {
                if (n.len > 0) {
                    c = 'N';
                    n.pos++;
                    n.len--;
                }
                if (n.len == 0) n = n_it != N.end() ? (Position) *n_it++ : Position{-1, -1};
            }

            if (c == '\0') break;

            if (low.pos == len) {
                if (low.len > 0) {
                    c = tolower(c);
                    low.pos++;
                    low.len--;
                }

                if (low.len == 0) low = low_it != LOW.end() ? (Position) *low_it++ : Position{-1, -1};
            }

            output << c;
            len++;
        }

        if (i == target_length) break;

        char c = decode(target[i]);
        if (low.pos == len) {
            if (low.len > 0) {
                c = tolower(c);
                low.pos++;
                low.len--;
            }

            if (low.len == 0)
                low = low_it != LOW.end() ? (Position) *low_it++ : Position{-1, -1};
        }

        output << c;
        len++;
    }

    output.close();

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Restoring target file finished in: " << duration.count() << " ms." << endl;
}

int main(int argc, char **argv) {
    auto start = high_resolution_clock::now();

    if (argc <= 1) {
        std::cerr << "No arguments!" << std::endl;
        return -1;
    }

    loadRefFile(argv[1]);

    parseCompressed(argv[2]);
    decompress(argv[3]);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cout << "Decompression: " << duration.count() << " ms.";
    return 0;
}

