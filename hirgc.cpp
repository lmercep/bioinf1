#include <iostream>
#include <vector>
#include <fstream>
#include "chrono"

using namespace std;
using namespace std::chrono;

typedef unsigned long long ll;

const int MAX_ARR_NUM_BIT = 26;
const int MAX_ARR_NUM = 1 << MAX_ARR_NUM_BIT;

// region auxiliary data (delta)
string ID;
typedef struct {
    int sequence_len;
    long num;
} SeqLen;
vector<SeqLen> SEQ_LEN;

typedef struct {
    int pos;
    int len;
} Position;

vector<Position> LOW;
vector<Position> N;

typedef struct {
    int pos;
    char ch;
} Others;
vector<Others> OTH;
// endregion

// the only algortihm parameter, length of the minimal match
int k = 15;

short reference[MAX_ARR_NUM];
short target[MAX_ARR_NUM];

int bucket_head[MAX_ARR_NUM];
int bucket_next[MAX_ARR_NUM];

int reference_length = 0, target_length = 0;

/**
 * Encodes a character to an integer value.
 * @param c character to encode
 * @return -1 if the encoding is not supported, 4 if the character is N
 * @author Mateja Iveta
 */
int encode(char c) {
    switch (c) {
        case 'A':
            return 0;
        case 'C':
            return 1;
        case 'G':
            return 2;
        case 'T':
            return 3;
        case 'N':
            return 4;
        default:
            return -1;
    }
}

/**
 * Loads the reference sequence from the specified file, preprocesses it and stores the clean reference in a global variable.
 * @param filename reference file
 * @author Mateja Iveta
 */
void loadRefFile(char *filename) {
    auto start = high_resolution_clock::now();

    ifstream input;
    input.open(filename, ios::in);

    if (!input.is_open()) {
        cerr << "Failed to open file '" << filename << "'. Bailing out." << endl;
        exit(-1);
    }

    string L, line;

    if (!getline(input, line)) {
        cout << "No sequence name. Bailing out." << endl;
        exit(-1);
    }

    while (getline(input, line)) {
        if (line.empty()) continue;

        for (char &i : line) {
            if (islower(i)) i = toupper(i);

            int encoded = encode(i);
            if (encoded < 0 || encoded == 4) continue;

            reference[reference_length++] = encoded;
        }
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Loading reference file finished in: " << duration.count() << " ms." << endl;
}

/**
 * Loads the target sequence from the specified file, generates the delta information
 * , and stores the clean target file in a global variable.
 * @param filename target file
 * @author Mateja Iveta
 */
void loadTargetFile(char *filename) {
    auto start = high_resolution_clock::now();

    target_length = 0;

    ifstream input;
    input.open(filename, ios::in);

    if (!input.is_open()) {
        cerr << "Failed to open file '" << filename << "'. Bailing out." << endl;
        exit(-1);
    }

    string line;
    if (!getline(input, line)) {
        cerr << "No sequence name. Bailing out." << endl;
        exit(-1);
    }

    ID = line.substr(1);

    int len = 0;

    Position low = {0, 0};
    Position n = {0, 0};

    while (getline(input, line)) {
        int seq_len = line.length();
        SeqLen seqLen;

        if (SEQ_LEN.empty()) {
            seqLen = {seq_len, 0};
            SEQ_LEN.push_back(seqLen);
        }

        seqLen = SEQ_LEN.back();
        if (seqLen.sequence_len != seq_len) {
            seqLen = {seq_len, 1};
            SEQ_LEN.push_back(seqLen);
        } else {
            seqLen.num += 1;
            SEQ_LEN.back() = seqLen;
        }


        for (int i = 0; i < seq_len; i++) {
            char c = line[i];

            if (islower(c)) {
                if (low.len == 0) low.pos = len;
                low.len++;

                c = toupper(c);
            } else {
                if (low.len > 0) {
                    LOW.push_back(low);
                    low.len = 0;
                }
            }

            if (c == 'N') {
                if (n.len == 0) n.pos = len;
                n.len++;
            } else {
                if (n.len > 0) {
                    N.push_back(n);
                    n.len = 0;
                }
            }

            int encoded = encode(c);
            if (encoded == -1) { // other character
                Others oth = {len, c};
                OTH.push_back(oth);
            } else if (encoded != 4) {
                target[target_length++] += encoded;
            }

            len++;
        }
    }

    if (low.len > 0) {
        LOW.push_back(low);
        low.len = 0;
    }

    if (n.len > 0) {
        N.push_back(n);
        n.len = 0;
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Loading target file finished in: " << duration.count() << " ms." << endl;
}

/**
 * Initializes the hash buckets and pointers from a clean reference array.
 * Requires the reference to be in a global variable.
 * @author Luka Mercep
 */
void reference_preprocessing() {
    // initialize bucket heads to point to blank
    for (int i = 0; i < MAX_ARR_NUM; i++) {
        bucket_head[i] = -1;
    }

    ll curr_gen_seq = 0;
    for (int i = k - 1; i >= 0; i--) {
        curr_gen_seq = curr_gen_seq << 2;
        curr_gen_seq += reference[i];
    }

    ll mod_value = curr_gen_seq & ((ll) (MAX_ARR_NUM - 1));
    bucket_next[0] = bucket_head[mod_value];
    bucket_head[mod_value] = 0;

    int shift = (k << 1) - 2;

    for (int index = 1; index <= reference_length - k; index++) {
        // update current gen sequence
        curr_gen_seq = curr_gen_seq >> 2;
        curr_gen_seq += ((ll) reference[index + k - 1]) << shift;

        mod_value = curr_gen_seq & ((ll) (MAX_ARR_NUM - 1));
        bucket_next[index] = bucket_head[mod_value];
        bucket_head[mod_value] = index;
    }
}

/**
 * Checks if the target array is clean.
 * @param err error message.
 * @author Luka Mercep
 */
void check_tar(string err) {
    for (int i = 0; i < target_length; i++) {
        if (target[i] < 0 || target[0] > 3) {
            cout << "Index: " << i << endl;
            cout << "Value: " << target[i] << endl;
            cout << "Error: " << err << endl;
            exit(0);
        }
    }

    cerr << "Check passed: " << err << endl;
}

/**
 * Matches the target sequence to the reference sequence, and appends the matching results to the specified file.
 * @param filename compressed file, appends to this file
 * @author Luka Mercep
 */
void matching_algorithm(char *filename) {
    auto start = high_resolution_clock::now();

    reference_preprocessing();

    ofstream output;
    output.open(filename, ios::app);
    output << "%" << endl;

    int last_unmached_index = 0;

    for (int target_index = 0; target_index <= target_length - k; target_index++) {
        ll curr_gen_seq = 0;
        for (int j = k - 1; j >= 0; j--) {
            curr_gen_seq = curr_gen_seq << 2; //curr_seq *= 4
            curr_gen_seq += ((ll) target[target_index + j]);
        }

        ll mod_value = curr_gen_seq & ((ll) (MAX_ARR_NUM - 1));
        int mapping_index = bucket_head[mod_value];

        if (mapping_index != -1) {
            int best_mapping_length = -1;
            int best_mapping_index = -1;

            while (mapping_index != -1) {
                // for this mapping calculate biggest match
                int offset = 0;
                int curr_ref_index = mapping_index;
                int curr_tar_index = target_index;

                while (curr_ref_index < reference_length && curr_tar_index < target_length &&
                       reference[curr_ref_index] == target[curr_tar_index]) {
                    offset++;
                    curr_ref_index++;
                    curr_tar_index++;
                }

                if (offset >= k && offset > best_mapping_length) {
                    best_mapping_length = offset;
                    best_mapping_index = mapping_index;
                }

                mapping_index = bucket_next[mapping_index];
            }

            // now with best print best matching

            if (best_mapping_length > -1) {
                // unmatched part of the target sequence
                if (last_unmached_index != 0) {
                    output << endl;
                }
                for (int i = last_unmached_index; i < target_index; i++) {
                    if (i < 0 || i >= target_length) {
                        cout << "trenutni i: " << i << endl;
                        cout << "pocetak: " << last_unmached_index << " kraj: " << target_index << endl;
                        exit(0);
                    }

                    if (target[i] < 0 || target[i] > 3) {
                        cout << "tar: " << target[i];
                        exit(0);
                    }
                    output << target[i];
                    if (i + 1 == target_index) output << endl;
                }

                // print match
                output << best_mapping_index + 1 << "," << best_mapping_length;

                last_unmached_index = target_index + best_mapping_length;
                target_index = last_unmached_index - 1;
            }
        }
    }

    if (last_unmached_index != 0) {
        output << endl;
    }
    // print end of the sequence (just unmatched part)
    for (int i = last_unmached_index; i < target_length; i++) {
        output << target[i];
    }

    output << endl;
    output << "%";

    output.close();

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Compression finished in: " << duration.count() << " ms." << endl;
}


/**
 * Initializes the delta information.
 * @author Mateja Iveta
 */
void initDelta() {
    SEQ_LEN.clear();
    LOW.clear();
    N.clear();
    OTH.clear();
}

/**
 * Prints the specified parts of the delta information to standard output.
 * @param id true if ID needs to be printed, false otherwise
 * @param seq_len true if sequence lengths need to be printed, false otherwise.
 * @param low true if the intervals of lowercase letters need to be printed, false otherwise.
 * @param n true if intervals of the letter 'N' need to be printed, false otherwise.
 * @param oth true if locations of other characters need to be printed, false otherwise.
 * @author Mateja Iveta
 */
void printDelta(bool id, bool seq_len, bool low, bool n, bool oth) {
    if (id) {
        cout << "ID = " << ID << endl;
    }

    if (seq_len) {
        cout << "Line sequence lengths =" << endl;
        for (vector<SeqLen>::const_iterator i = SEQ_LEN.begin(); i != SEQ_LEN.end(); ++i) {
            cout << i->sequence_len << "," << i->num << " ";
        }
        cout << endl;
    }

    if (low) {
        cout << "Lowercase letter sequences (pos, len) =" << endl;
        for (vector<Position>::const_iterator i = LOW.begin(); i != LOW.end(); ++i) {
            Position p = *i;
            cout << "(" << p.pos << ", " << p.len << ") ";
        }
        cout << endl;
    }

    if (n) {
        cout << "Positions of the letter N (pos, len) =" << endl;
        for (vector<Position>::const_iterator i = N.begin(); i != N.end(); ++i) {
            Position p = *i;
            cout << "(" << p.pos << ", " << p.len << ") ";
        }
        cout << endl;
    }

    if (oth) {
        cout << "Other characters (pos, ch) =" << endl;
        for (vector<Others>::const_iterator i = OTH.begin(); i != OTH.end(); ++i) {
            Others oth = *i;
            cout << "(" << oth.pos << ", " << oth.ch << ") ";
        }
        cout << endl;
    }
}


/**
 * Stores the delta information in the specified file. If such file exists, it will overwrite current data!
 * @param filename
 * @return
 * @author Mateja Iveta
 */
int storeDelta(char *filename) {
    auto start = high_resolution_clock::now();

    ofstream output;
    output.open(filename, ios::out);

    if (!output.is_open()) {
        cerr << "Failed to open file '" << filename << "'. Bailing out." << endl;
        return -1;
    }

    output << ID << endl; // Line 1: target sequence identificator

    for (SeqLen i : SEQ_LEN) {
        output << i.sequence_len << "," << i.num << " ";
    }
    output << endl; // Line 2: lengths of short sequences

    for (auto p : LOW) {
        output << p.pos << "," << p.len << " ";
    }
    output << endl; // Line 3: pairs position,length of lowercase letters

    for (auto p : N) {
        output << p.pos << "," << p.len << " ";
    }
    output << endl; // Line 4: pairs position,length of the letter N

    for (auto oth : OTH) {
        output << oth.pos << "," << oth.ch << " ";
    }
    output << endl; // Line 5: pairs position,character of other characters

    output.close();

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cerr << "Storing delta data finished in: " << duration.count() << " ms." << endl;
    return 0;
}


int main(int argc, char **argv) {
    auto start = high_resolution_clock::now();

    if (argc <= 1) {
        std::cerr << "No arguments!" << std::endl;
        return -1;
    }


    loadRefFile(argv[1]);
    loadTargetFile(argv[2]);

    storeDelta(argv[3]);
    matching_algorithm(argv[3]);


    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);


    cout << "Compression: " << duration.count() << " ms.";
    return 0;
}

