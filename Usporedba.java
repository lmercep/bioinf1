import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Usporedba {

    public static void main(String[] args) throws IOException {
        String nasFile = args[0];
        String originalFile = args[1];

        BufferedReader nasBr = new BufferedReader(new FileReader(nasFile));
        BufferedReader orgBr = new BufferedReader(new FileReader(originalFile));

        int index = 1;

        while (true) {
            String nasLine = nasBr.readLine();
            String orgLine = orgBr.readLine();

            if (nasLine == null && orgLine == null) {
                System.out.println("Komparacija uspjesno obavljena");
                break;
            }
            if (nasLine == null) {
                System.out.println("Nas file je kraci");
                break;
            }
            if (orgLine == null) {
                System.out.println("Originalni file je kraci");
                break;
            }

            if (!nasLine.equals(orgLine)) {
                System.out.println("Razlikuju se na " + index + "-toj liniji");
                System.out.println("Nasa linija: ");
                System.out.println(nasLine);
                System.out.println("Originalna linija: ");
                System.out.println(orgLine);
                break;
            }

            index++;
        }
    }
}
